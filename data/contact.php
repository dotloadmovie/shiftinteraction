<?php

$page = new stdClass();

$page->title = 'Contact';

$page->detail = '<p>Email: <a data-external="true" href="mailto:info@shiftinteraction.com">info@shiftinteraction.com</a></p><p>GitHub: <a href="http://githuv.com/dotloadmovie" data-external="true" target="_blank">dotloadmovie</a></p><p>Twitter: <a data-external="true" href="http://twitter.com/dotloadmovie" target="_blank">@dotloadmovie</a></p><p>LinkedIn: <a data-external="true" href="http://uk.linkedin.com/in/davetickle/" target="_blank">http://uk.linkedin.com/in/davetickle/</a></p>';

?>