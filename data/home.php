<?php

$page = new stdClass();

$page->title = 'Home';

$page->detail = '<p>
Shift Interaction is the freelance imprint of Dave Tickle, an experienced front-end web developer and user-interface engineer. I have 14 years of commercial experience in the building of small, medium and enterprise sites and web applications and a full-stack skillset encompassing industry standard technologies.
</p>

<p>
I\'ve worked on applications for a large number of international companies, creative agencies and technology consultancies, from individual brand microsites and Facebook apps to large responsive sites and complex, data-driven web applications. Typically I work as a senior or lead developer on a project as part of a multidisciplinary team - usually in an Agile (Scrum) style environment.
</p>

<p>
    Over the course of my career I\'ve worked on projects for clients including Sony, Paramount Pictures, TalkTalk, McDonalds, Gillette, Unilever and Disney. I\'ve built many websites of course, but also games, experiential rigs, proxy frameworks for cross-system communication, UGC communities and video compositing tools. Using at least some of the following:
</p>

<h3>
Markup and styling
</h3>
<p>HTML5 (including Video, Audio and Canvas), CSS3, SASS, media queries</p>

<h3>
JavaScript
</h3>
<p>jQuery, Backbone, Require (AMD library), CreateJS (Rich media library), Grunt</p>

<h3>
Flash
</h3>
<p>Flash CS, Flex, OO AS3, FlashDevelop, TweenMax</p>

<h3>
API Integration
</h3>
<p>Facebook, YouTube, Twitter, REST API consumption</p>

<h3>
Server-side
</h3>
<p>LAMP stack (LInux, Apache, MySQL, OO PHP5), Wordpress, server admin, AWS configuration, Git, Subversion</p>';

?>
