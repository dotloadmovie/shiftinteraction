<?php

class ImgData{

    function __construct($imgname, $title, $client, $desc){

        $this->img = $imgname;
        $this->title = $title;
        $this->client = $client;
        $this->description = $desc;

    }

}

$page = new stdClass();

$page->title = 'Portfolio';

$page->detail = '<p>I\'ve worked on projects of all scales, within large and small teams within creative agencies, consultancies and startups. A small list of recent projects I\'ve contributed to are shown below:</p>';

$page->images = array(

    new ImgData('2.jpg', 'CapturedHQ', 'Blenheim Chalcot Copperdime Systems', 'Practical knowledge capture, storage, retrieval and scheduling application. Large scale SPA built using Backbone/Require stack with heavy use of OO JS, HTML5 and CSS3. Consuming a Python REST API'),
    new ImgData('3.jpg', 'Wickstead Developments', 'Kook Design', 'Graphic driven portfolio website built in Backbone/Require/HTML5 with extensive use of tweening animations and full bleed imagery'),
    new ImgData('4.jpg', 'Sony Make Believe - Priest', 'The App Business', 'Facebook connected photo manipulation application built in Flash/AS3 with a PHP5 data backend'),
    new ImgData('1.jpg', 'Boxby - NBC video quiz', 'The App Business', 'Facebook video quiz app built on HTML5 Video using the Joose JS Framework with a PHP5 data layer'),
    new ImgData('5.jpg', 'Dove Messaging App', 'The App Business', 'Animated messaging framework built using HTML5 sprite animation using the Joose framework. PHP5 data backend'),
    new ImgData('6.jpg', '[Application not yet live]', 'Direct Client', 'Education testing and progress monitoring application. Administration system built in Backbone/Require and HTML5. Responsive client system built in Joose and HTML5. Both consuming and communicating via a Python REST API'),
    new ImgData('8.jpg', 'Electrocomponents', 'SAS London', 'Corporate website skin - built in HTML5/CSS/jQuery for an ASP.NET CMS layer with graceful degradation for older browsers'),
    new ImgData('9.jpg', 'Home Retail Group', 'SAS London', 'Corporate website skin - built in HTML5/CSS/jQuery for an ASP.NET CMS layer with graceful degradation for older browsers'),
    new ImgData('10.jpg', 'Talk Talk - XFactor', 'The App Business', 'Live video compositing tool and associated website. Built in Flash/AS3, Joose, HTML5, CSS3')
);

?>
