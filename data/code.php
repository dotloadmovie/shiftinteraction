<?php

$page = new stdClass();

$page->title = 'Code';

$page->detail = '<p>For the sake of completeness, you can view the codebase for the current web site using the resource below. The code is stored on a public-facing git repo at BitBucket and displayed using Miroslav Magda\'s excellent <a data-external="true" target="_blank" href="https://bitbucket.org/miroslavmagda/repo-bitbucket.js/overview">repo-bitbucket.js</a>.</p>';

?>