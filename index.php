<!doctype html>

<head>

  <meta charset="utf-8">

    <?php
        include('inc/header.inc.php');
    ?>

    <style type="text/css" id="no_script">

        *{
            border: none;
            margin: 0;
            padding: 0;
        }

        body{
            background: #000;
            color: #999;
            font-size: 62.5%;
            font-family: arial, sans-serif;
        }

        .left_col{
            width: 600px;
            margin: 20px auto;
        }

        img{
            margin: 0 0 10px 0;
        }

        h3{
            font-size: 1.8em;
        }

        p{
            font-size: 1.2em;
            margin: 0 0 15px 0;
        }

        a{
            color: #fff;

        }

        .footer{
            display: none;
        }


    </style>


</head>
<body>

<div class="container">

    <div class="header">


    </div>

    <div class="content">

        <div class="content_inner">

            <div class="outer_col">

                <div class="left_col" id="left_col">

                    <img src="/img/global/logo.png" />


                    <?php

                        require('data/home.php');

                        echo $page->detail;

                    ?>


                    <p><a href="mailto:info@shiftinteraction.com">info@shiftinteraction.com</a></p>



                </div>

                <script type="text/javascript">
                    document.getElementById('left_col').innerHTML = '';
                </script>

                <div class="right_col">



                </div>

                <br style="clear:both;float: left;" />

            </div>

            <br style="clear:both;float: left;" />


        </div>


    </div>

    <div class="footer">

        <div class="footer_inner">

            <div class="footer_col_left">

                <h3>Powered By:</h3>

                <p>
                    <a href="#">BackboneJS</a>, <a href="#">RequireJS</a>, <a href="#">UnderscoreJS</a><br />
                    <a href="https://github.com/BoilerplateMVC/Backbone-Require-Boilerplate" target="_blank">BRB</a><br />
                    <a href="http://www.jquery.com" target="_blank">jQuery</a><br />
                    <a href="http://www.php.net" target="_blank">PHP</a>

                </p>

            </div>


        </div>

    </div>

</div>



</body>
</html>