<?php

class GetPage {

    function __construct($pagename){

        switch($pagename){

            case 'home': include '../data/home.php'; break;
            case 'portfolio': include '../data/portfolio.php'; break;
            case 'code': include '../data/code.php'; break;
            case 'contact': include '../data/contact.php'; break;

            default: '../data/home.php'; break;

        }

        $this->pagecontent = $page;

    }

    function parsePage(){

        return json_encode($this->pagecontent);

    }

}

$parser = new GetPage($_GET['page']);

echo $parser->parsePage();

?>