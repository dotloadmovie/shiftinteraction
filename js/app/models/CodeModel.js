// Model.js
// --------
define(["jquery", "backbone", "models/Model"],

    function($, Backbone, Model) {

        // Creates a new Backbone Model class object
        var CodeModel = Model.extend({

            // Model Constructor,
            initialize: function() {

                this.urlRoot = '/services/getPage.php?page=code';

            }


        });

        // Returns the Model class
        return CodeModel;

    }

);