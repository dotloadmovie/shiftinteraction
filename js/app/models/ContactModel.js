// Model.js
// --------
define(["jquery", "backbone", "models/Model"],

    function($, Backbone, Model) {

        // Creates a new Backbone Model class object
        var ContactModel = Model.extend({

            // Model Constructor,
            initialize: function() {

                this.urlRoot = '/services/getPage.php?page=contact';

            }


        });

        // Returns the Model class
        return ContactModel;

    }

);