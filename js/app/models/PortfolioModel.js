// Model.js
// --------
define(["jquery", "backbone", "models/Model"],

    function($, Backbone, Model) {

        // Creates a new Backbone Model class object
        var PortfolioModel = Model.extend({

            // Model Constructor,
            initialize: function() {

                this.urlRoot = '/services/getPage.php?page=portfolio';

            }


        });

        // Returns the Model class
        return PortfolioModel;

    }

);
