// DesktopRouter.js
// ----------------
define(["jquery", "backbone", "models/Model", "views/View", "collections/Collection"],

    function($, Backbone, Model, View, Collection) {

        var DesktopRouter = Backbone.Router.extend({

            initialize: function() {

                $('#no_script').remove();

                var _self = this;

                // Tells Backbone to start watching for hashchange events
                Backbone.history.start({pushState: true});

                this.header();
                this.sidebar();

                $(document).on('click', 'a:not([data-external])', function(e){

                    e.preventDefault();

                    _self.navigate($(this).attr('href'), {trigger: true});


                });

            },

            header: function(){

                var _self = this;

                require(['views/Header'], function(HeaderView){

                    _self.headerView = new HeaderView({el:$('.header:first')});

                });

            },

            sidebar: function(){

                var _self = this;

                require(['views/SidebarView', 'models/PortfolioModel'], function(SidebarView, PortfolioModel){

                    if(_self.portfolioModel === undefined){
                        _self.portfolioModel = new PortfolioModel({urlRoot: '/portfolio', pagename: 'portfolio'});
                    }

                    _self.sidebarView = new SidebarView({el:$('.right_col:first'),  model: _self.portfolioModel});

                });


            },


            // All of your Backbone Routes (add more)
            routes: {

                // When there is no hash on the url, the home method is called
                "": "index",
                "about":"index",
                "portfolio":"portfolio",
                "portfolio/:id":"portfolio",
                "contact":"contact",
                "code":"code"

            },

            index: function() {

                var _self = this;

                this.resetView('Home');

                require(['views/HomeView', 'models/HomeModel'], function(HomeView, HomeModel){

                    if(_self.homeModel === undefined){
                        _self.homeModel = new HomeModel({urlRoot: '/home', pagename: 'home'});
                    }

                    _self.currView = new HomeView({el:$('.left_col:first'), model: _self.homeModel});

                });

            },

            about: function(){



            },

            portfolio: function(id){

                var _self = this, _id = id;

                if(id === undefined){
                    _id = 0;
                }


                this.resetView('Portfolio');

                require(['views/PortfolioView', 'models/PortfolioModel'], function(PortfolioView, PortfolioModel){

                    if(_self.portfolioModel === undefined){
                        _self.portfolioModel = new PortfolioModel({urlRoot: '/portfolio', pagename: 'portfolio'});
                    }

                    _self.currView = new PortfolioView({el:$('.left_col:first'), model: _self.portfolioModel, start:_id});

                });


            },

            contact: function(){

                var _self = this;

                this.resetView('Contact');

                require(['views/ContactView', 'models/ContactModel'], function(ContactView, ContactModel){

                    if(_self.contactModel === undefined){
                        _self.contactModel = new ContactModel({urlRoot: '/contact', pagename: 'contact'});
                    }

                    _self.currView = new ContactView({el:$('.left_col:first'), model: _self.contactModel});

                });


            },

            code: function(){

                var _self = this;

                this.resetView('Code');

                require(['views/CodeView', 'models/CodeModel'], function(CodeView, CodeModel){

                    if(_self.codeModel === undefined){
                        _self.codeModel = new CodeModel({urlRoot: '/code', pagename: 'code'});
                    }

                    _self.currView = new CodeView({el:$('.left_col:first'), model: _self.codeModel});

                });

            },


            resetView: function(title){

                $('title').html('Shift Interaction | '+title);
            }

        });

        // Returns the DesktopRouter class
        return DesktopRouter;

    }

);