// HomeView.js
// -------
define(["jquery", "backbone", "text!templates/sidebar.html"],

    function($, Backbone, Template){

        var SidebarView = Backbone.View.extend({


            initialize: function(){

                this.model.on('change', this.render, this);

                if(this.model.has('title')){

                    this.render();
                }
                else{

                    this.model.fetch();
                }

            },

            render: function(){

                this.sidebar = _.template(Template, {model:this.model});

                this.$el.html(this.sidebar);

            }


        });

        return SidebarView;

    }

)