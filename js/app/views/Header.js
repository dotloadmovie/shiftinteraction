// HomeView.js
// -------
define(["jquery", "backbone", "text!templates/header.html"],

    function($, Backbone, Template){

        var HeaderView = Backbone.View.extend({


            initialize: function(){

                this.render();


            },

            render: function(){

                this.header = _.template(Template, {});

                this.$el.html(this.header);

            }


        });

        return HeaderView;

    }

)