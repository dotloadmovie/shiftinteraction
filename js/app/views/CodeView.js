// Home.js
// -------
define(["jquery", "backbone", "views/View", "text!templates/code.html", "libs/repo-bitbucket.js"],

    function($, Backbone, View, Template){

        var CodeView = View.extend({


            initialize: function(){

                this.model.on('change', this.render, this);

                if(this.model.has('title')){
                    this.render();
                }
                else{
                    this.model.fetch();
                }


            },

            render: function(){

                this.template = _.template(Template, {model:this.model});

                this.$el.html(this.template);

                this.$el.find('#repo_container').bitbucket({user : 'dotloadmovie', repository : 'shiftinteraction', revision : 'master'});

            }


        });

        return CodeView;

    }

)