// Home.js
// -------
define(["jquery", "backbone", "views/View", "text!templates/portfolio.html"],

    function($, Backbone, View, Template){

        var PortfolioView = View.extend({


            initialize: function(options){

                this.model.on('change', this.render, this);
                this.start = options.start;

                if(this.model.has('title')){
                    this.render();
                }
                else{
                    this.model.fetch();
                }




            },

            render: function(){

                this.template = _.template(Template, {model: this.model});

                this.$el.html(this.template);

               // this.setupCycle();


            },

            setupCycle: function(){

                this.$el.find('.can_cycle:first').cycle({

                    fx:'scrollHorz',
                    slides: '.cycle_element',
                    timeout: 0,
                    prev:'.portfolio_prev',
                    next: '.portfolio_next',
                    startingSlide:this.start

                });



            }


        });

        return PortfolioView;

    }

)