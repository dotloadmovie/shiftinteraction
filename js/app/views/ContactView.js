// Home.js
// -------
define(["jquery", "backbone", "views/View", "text!templates/contact.html"],

    function($, Backbone, View, Template){

        var ContactView = View.extend({


            initialize: function(){

                this.model.on('change', this.render, this);

                if(this.model.has('title')){
                    this.render();
                }
                else{
                    this.model.fetch();
                }


            },

            render: function(){

                this.template = _.template(Template, {model:this.model});

                this.$el.html(this.template);


            }


        });

        return ContactView;

    }

)