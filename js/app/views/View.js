// View.js
// -------
define(["jquery", "backbone", "models/Model"],

    function($, Backbone, Model, template){

        var View = Backbone.View.extend({

            // View constructor
            initialize: function() {

                // Calls the view's render method
                this.render();

            },

            // View Event Handlers
            events: {

            },

            // Renders the view's template to the UI
            render: function() {

                // Maintains chainability
                return this;

            }

        });

        // Returns the View class
        return View;

    }

);