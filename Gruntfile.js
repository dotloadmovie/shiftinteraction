module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        requirejs: {
            mobileJS: {
                options: {
                    baseUrl: "js/",
                    paths: {
                        "mobile": "app/init/MobileInit"
                    },
                    wrap: true,
                    name: "libs/almond",
                    preserveLicenseComments: false,
                    optimize: "uglify",
                    optimizeCss: "standard",
                    mainConfigFile: "js/app/config/config.js",
                    include: ["mobile"],
                    out: "public/js/app/init/MobileInit.min.js"
                }
            },
            mobileCSS: {
                options: {
                    optimizeCss: "standard",
                    cssIn: "./css/mobile.css",
                    out: "./css/mobile.min.css"
                }
            },
            desktopJS: {
                options: {
                    baseUrl: "js/",
                    paths: {
                        "desktop": "app/init/DesktopInit"
                    },
                    wrap: true,
                    name: "libs/almond",
                    preserveLicenseComments: false,
                    optimize: "uglify",
                    findNestedDependencies: true,
                    mainConfigFile: "js/app/config/config.js",
                    include: ["desktop"],
                    out: "js/app/init/DesktopInit.min.js"
                }
            },
            desktopCSS: {
                options: {
                    optimizeCss: "standard",
                    cssIn: "./css/core.css",
                    out: "./css/core.min.css"
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'public/js/app/**/*.js', '!public/js/app/**/*min.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: false,
                    module: true,
                    document: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.registerTask('test', ['jshint']);
    grunt.registerTask('build', ['requirejs:desktopJS','requirejs:desktopCSS']);
    grunt.registerTask('default', ['build']);

};
